# pre-commit-hooks

A collection of git pre-commit hooks for the [pre-commit](https://pre-commit.com/) framework.

## Configuration

Install `pre-commit`:

```
pip install pre-commit
```

Add a file called `.pre-commit-config.yaml` to the root of your project. The pre-commit config file describes what repositories and hooks are installed.

To use the `clear-notebook` hook from this repo, put this into your `.pre-commit-config.yaml`:

```yaml
repos:
-   repo: https://gitlab.com/coobas/pre-commit-hooks
    rev: v0.2.1
    hooks:
    - id: clear-notebook
```

## Usage


### Preparation

Run `pre-commit install` to install pre-commit into the git hooks of your current repository. `pre-commit` will now *run on every commit*.


### Manual run

If you want to *manually* run all pre-commit hooks on a repository, run `pre-commit run --all-files`. To run individual hooks use `pre-commit run <hook_id>`.


### Committing

From now on, every time you try to `git commit`, `pre-commit` will check *stashed* Jupyter notebook files.
In case there are any outputs in the notebook, the commit will fail and your notebook file will be overwritten by a clear one.
You should observe something like this:

```
▶ git commit
clear notebook...........................................................Failed
hookid: clear-notebook

Files were modified by this hook. Additional output:

my-notebook.ipynb
my-notebook.ipynb contains outputs
```

```
▶ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   my-notebook.ipynb

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   my-notebook.ipynb
```

You can then `git add` (yes, add again) the modified file(s) and `git commit` should be happy.


### Updating the hooks

pre-commit configuration aims to give a repeatable and fast experience and therefore intentionally doesn't provide facilities for "unpinned latest version" for hook repositories.

Instead, pre-commit provides tools to make it easy to upgrade to the latest versions with `pre-commit autoupdate`.
