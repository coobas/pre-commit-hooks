from typing import Iterable
import copy
import io
import json
import os
import sys
import tempfile

import click
import nbconvert.preprocessors
import nbformat



@click.command()
@click.option('--no-overwrite', '-n', is_flag=True, help='Will not overwrite the files')
@click.option('--clear-meta', '-m', is_flag=True, help='Clear all metadata as well')
@click.argument('files', required=True, metavar='path', nargs=-1)
def main(no_overwrite: bool, clear_meta: bool, files: Iterable[str]) -> int:
    retv = 0

    processor = nbconvert.preprocessors.ClearOutputPreprocessor()

    if not clear_meta:
        processor.remove_metadata_fields.clear()

    for filename in files:
        click.echo('Checking {}'.format(filename))

        with open(filename, 'r+') as notebok_file:
            input_notebook = nbformat.read(notebok_file, as_version=nbformat.NO_CONVERT)
            clear_notebook, _resources = processor.preprocess(copy.deepcopy(input_notebook), {})
            if input_notebook != clear_notebook:
                print('{} contains outputs'.format(filename))
                retv += 1
                if not no_overwrite:
                    notebok_file.truncate(0)
                    notebok_file.seek(0)
                    nbformat.write(clear_notebook, notebok_file)

    return retv


if __name__ == '__main__':
    exit(main())  # pylint: disable=no-value-for-parameter
