from setuptools import find_packages
from setuptools import setup


setup(
    name='pre_commit_hooks',
    url='https://gitlab.com/coobas/pre-commit-hooks',
    version='0.2.1',

    author='Kuba Urban',

    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],

    packages=find_packages(exclude=('tests*', 'testing*')),
    install_requires=[
        'nbconvert',
        'nbformat',
        'click',
    ],
    entry_points={
        'console_scripts': [
            'clear-notebook = hooks.clear_notebook:main',
        ],
    },
)
